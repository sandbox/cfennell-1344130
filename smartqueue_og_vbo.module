<?php

/**
 * Implementation of hook_menu()
 */
function smartqueue_og_vbo_menu() {
  $items['admin/settings/smartqueue-og-vbo'] = array(
    'title' => t('Smartqueues for Organic Groups VBO'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('smartqueue_og_vbo_settings'),
    'access arguments' => array('administer smartqueue for og vbo'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * @see smartqueue_og_vbo_menu()
 */
function smartqueue_og_vbo_settings() {
  $form = array();
  $views = views_get_all_views();
  $options = array();
  foreach ($views as $key => $view) {
    $options[$key] = $view->name;
  }
  $form['smartqueue_og_vbo_member_view'] = array(
    '#type' => 'select',
    '#title' => t('Group Memember View'),
    '#description' => t('The view you want to display to group members.'),
    '#options' => array(0 => t('None')) + $options,
    '#default_value' => variable_get('smartqueue_og_vbo_member_view', 'smartqueue_og_vbo'),
    '#required' => TRUE,
  );
  $form['smartqueue_og_vbo_nonmember_view'] = array(
    '#type' => 'select',
    '#title' => t('Non-group Memember View'),
    '#description' => t('The view you want to display to non-group members.'),
    '#options' => array(0 => t('None')) + $options,
    '#default_value' => variable_get('smartqueue_og_vbo_nonmember_view', 'smartqueue_og_default'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_perm().
 */
function smartqueue_og_vbo_perm() {
  return array('administer smartqueue for og vbo');
}

/**
 * Implementation of hook_action_info().
 */
function smartqueue_og_vbo_action_info() {

  //TODO: is there better way to hide these actions? hooks?
  global $user;
  if (count($user->og_groups) > 1) {
    $actions['smartqueue_og_vbo_copy_action'] = array(
      'description' => t('Copy Items to Another Group'),
      'type' => 'node',
      'configurable' => TRUE,
      'hooks' => array(),
      'behavior' => array('views_node_property'), // For Views Bulk Operations module
      'aggregate' => TRUE, // For Views Bulk Operations module
    );
    $actions['smartqueue_og_vbo_move_action'] = array(
      'description' => t('Move Items from this Group to Another Group'),
      'type' => 'node',
      'configurable' => TRUE,
      'hooks' => array(),
      'behavior' => array('views_node_property'), // For Views Bulk Operations module
      'aggregate' => TRUE, // For Views Bulk Operations module
      );
  }
  $actions['smartqueue_og_vbo_remove_action'] = array(
    'description' => t('Remove Items from this Group'),
    'type' => 'node',
    'configurable' => FALSE,
    'hooks' => array(),
    'behavior' => array('views_node_property'), // For Views Bulk Operations module
    'aggregate' => TRUE, // For Views Bulk Operations module
    );

  return $actions;
}

/**
 * @see smartqueue_og_vbo_action_info
 */
function smartqueue_og_vbo_move_action($nodes, $context) {
  smartqueue_og_vbo_copy_action($nodes, $context);
  smartqueue_og_vbo_remove_action($nodes, $context);
}

/**
 * @see smartqueue_og_vbo_action_info
 */
function smartqueue_og_vbo_move_action_form($context) {
  $form = array();
    //get all group queues except for the current one
    $form += smartqueue_og_vbo_get_user_groups();
  return $form;
}

/**
 * @see smartqueue_og_vbo_action_info
 */
function smartqueue_og_vbo_remove_action($nodes, $context) {
  foreach ($nodes as $nid) {
    foreach ($context['view']['arguments'] as $group_nid) {
      $queues = smartqueue_og_all_group_subqueues();
      nodequeue_subqueue_remove_node($queues[$group_nid], $nid);
    }
  }
}

/**
 * @see smartqueue_og_vbo_action_info
 */
function smartqueue_og_vbo_copy_action($nodes, $context) {
  foreach ($nodes as $nid) {
    $qid = smartqueue_og_get_qid();
    $queue = nodequeue_load($qid);
    foreach ($context['qids'] as $subqid) {
      $subqueue = nodequeue_load_subqueue($subqid);
      nodequeue_subqueue_add($queue, $subqueue, $nid);
    }
  }
}

/**
 * @see smartqueue_og_vbo_action_info
 */
function smartqueue_og_vbo_copy_action_form($context) {
  $form = array();
    //get all group queues except for the current one
  $form += smartqueue_og_vbo_get_user_groups();
  return $form;
}

/**
 * Convenience function to get all groups for a given user
 */
function smartqueue_og_vbo_get_user_groups() {
  $form = array();
  $user_queues = smartqueue_og_vbo_user_queues();
  if (count($user_queues)) {
    $form['qids'] = array(
      '#type' => 'select',
      '#title' => t('Groups'),
      '#options' => $user_queues,
      '#description' => t('Select the groups to add to the node.'),
      '#required' => TRUE,
      '#multiple' => TRUE,
    );
  }
  else {
    drupal_set_message(t('Please <a href="!url">create</a> a group first.', array('!url' => url('admin/content'))));
  }
  return $form;
}

/**
 * Submit handler for Add to Nodequeues action configuration.
 */
function smartqueue_og_vbo_copy_action_submit($form, &$form_state) {
  return array(
    'qids' => $form_state['values']['qids'],
  );
}

/**
 * Submit handler for Add to Nodequeues action configuration.
 */
function smartqueue_og_vbo_move_action_submit($form, &$form_state) {
  return array(
    'qids' => $form_state['values']['qids'],
  );
}

/**
 * Get queues for groups for which this user is a member
 *
 * @return array $qid => $title
 */
function smartqueue_og_vbo_user_queues() {
  global $user;
  $groups = (is_array($user->og_groups) && !empty($user->og_groups)) ? $user->og_groups : array();
  $queues = smartqueue_og_all_group_subqueues();
  $user_queues = array();
  foreach ($groups as $nid => $info) {
    if (($qid = $queues[$nid]) && $nid != arg(1) && $info['is_active'] == 1) {
      //TODO: do we need check_plain here? Check og to see if so.
      $user_queues[$qid] =  check_plain($info['title']);
    }
  }
  return $user_queues;
}

/**
 * Implementation of hook_block()
 */
function smartqueue_og_vbo_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;
  $block = array();
  if (module_exists('views')) {
    switch ($op) {
      case 'list':
        $blocks[0]['info'] = t("Group VBO Queue");
        return $blocks; 
        break;
      case 'view':
        if ($delta == 0) {
          if ($node = og_get_group_context()) {
            // TODO: add this feature: if count($user->og_groups) == 1, show a
            // view w/out the move/copy actions. Just show the remove action
            if (in_array($node->nid, array_keys($user->og_groups))) {
              $view = views_embed_view(variable_get('smartqueue_og_vbo_member_view', 'smartqueue_og_vbo'), 'default', $node->nid);
            }
            else {
              $view = views_embed_view(variable_get('smartqueue_og_vbo_nonmember_view', 'smartqueue_og_default'), 'default', $node->nid);
            }
            $block = array(
              'subject' => t("@title's Queue", array('@title' => $node->title)),
              'content' => $view,
            );
          }
        }
        return $block;
        break;
    }
  }
}

/**
 * Implementation of hook_views_api()
 */
function smartqueue_og_vbo_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'smartqueue_og_vbo') .'/includes/views',
  );
}
